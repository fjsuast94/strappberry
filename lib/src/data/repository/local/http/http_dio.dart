import 'package:strappberry/src/data/model/response.dart';
import 'package:dio/dio.dart';

class HttpDio {
  final Dio _dio;

  HttpDio({
    required String baseUrl,
    required Dio dio,
  }) : _dio = dio {
    dio.options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 10000,
      receiveTimeout: 10000,
      headers: _setHeaders,
    );
  }

  Future<ResponseModel> post({required route, data = const {}}) async {
    final ResponseModel resquest = ResponseModel(
      status: false,
      message: '',
    );
    try {
      final Response response = await _dio.post(route, data: data);
      if (response.statusCode == 200) {
        final ResponseModel body = ResponseModel.fromJson(response.data);
        if (body.status!) {
          resquest.status = body.status;
          resquest.data = body.data;
          resquest.message = body.message;
        } else {
          resquest.message = body.message;
        }
      }
    } on DioError catch (e) {
      //print(e.response?.data);
      resquest.message =
          "${_getResponseCode(e.response?.statusCode.toString())} \n ${e.error.toString()}";
    }
    return resquest;
  }

  Future<ResponseModel> get({required route, data = const {}}) async {
    final ResponseModel resquest = ResponseModel(
      status: false,
      message: '',
    );
    try {
      final Response response = await _dio.get(route);
      if (response.statusCode == 200) {
        final ResponseModel body = ResponseModel.fromJson(response.data);
        if (body.status!) {
          resquest.status = body.status;
          resquest.data = body.data;
          resquest.message = body.message;
        } else {
          resquest.message = body.message;
        }
      }
    } on DioError catch (e) {
      resquest.message =
          "${_getResponseCode(e.response?.statusCode.toString())} \n ${e.error.toString()}";
    }
    return resquest;
  }

  Future<ResponseModel> postFormData({required route, data = const {}}) async {
    final ResponseModel resquest = ResponseModel(
      status: false,
      message: '',
    );
    try {
      FormData formData = FormData.fromMap(data);
      final Response response = await _dio.post(route, data: formData);
      if (response.statusCode == 200) {
        final ResponseModel body = ResponseModel.fromJson(response.data);
        if (body.status!) {
          resquest.status = body.status;
          resquest.data = body.data;
          resquest.message = body.message;
        } else {
          resquest.message = body.message;
        }
      }
    } on DioError catch (e) {
      resquest.message =
          "${_getResponseCode(e.response?.statusCode.toString())} \n ${e.error.toString()}";
    }
    return resquest;
  }

  Future<ResponseModel> requestPost({required route, data = const {}}) async {
    final ResponseModel resquest = ResponseModel(
      status: false,
      message: '',
    );
    try {
      final Response response = await _dio.post(route, data: data);
      if (response.statusCode == 200) {
        resquest.status = true;
        resquest.data = response.data;
      }
    } on DioError catch (e) {
      resquest.message =
          "${_getResponseCode(e.response?.statusCode.toString())} \n ${e.error.toString()}";
    }
    return resquest;
  }

  Future<ResponseModel> requestGet({required route}) async {
    final ResponseModel resquest = ResponseModel(
      status: false,
      message: '',
    );
    try {
      final Response response = await _dio.get(route);
      if (response.statusCode == 200) {
        resquest.status = true;
        resquest.data = response.data;
      }
    } on DioError catch (e) {
      resquest.message =
          "${_getResponseCode(e.response?.statusCode.toString())} \n ${e.error.toString()}";
    }
    return resquest;
  }

  final Map<String, String> _setHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };

  String _getResponseCode(String? code) {
    Map<String, dynamic> codes = {
      "400": "bad-request",
      "405": "method-not-allowed",
    };
    return codes[code] ?? "Contenido desconocido";
  }
}
