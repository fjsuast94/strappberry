import 'package:get_storage/get_storage.dart';
import 'package:strappberry/src/data/model/favorite.dart';
import 'package:strappberry/src/data/model/kart.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/user.dart';

class StorageRepository {
  StorageRepository({
    required GetStorage storage,
  }) : _storage = storage;

  final GetStorage _storage;

  // Métodos de getStorage
  Future<void> clear() async {
    final boarding = onboarding;
    await _storage.erase();
    onboarding = boarding;
  }

  // Setter y getters de datos
  //'https://autocinemaimperial.com';

  set server(String valor) => _storage.write('server', valor);
  String get server => _storage.read('server') ?? '';

  set serverSocket(String valor) => _storage.write('serverSocket', valor);
  String get serverSocket => _storage.read('serverSocket') ?? '$server:3002';

  set api(String valor) => _storage.write('api', valor);
  String get api => _storage.read('api') ?? "$server/api";

  set onboarding(bool value) => _storage.write('onboarding', value);
  bool get onboarding => _storage.read('onboarding') ?? false;

  set page(String valor) => _storage.write('page', valor);
  String get page => _storage.read('page') ?? '/register-home';

  set canBack(bool valor) => _storage.write('canBack', valor);
  bool get canBack => _storage.read('page') ?? false;

  set auth(bool valor) => _storage.write('auth', valor);
  bool get auth => _storage.read('auth') ?? false;

  set biometric(bool valor) => _storage.write('biometric', valor);
  bool get biometric => _storage.read('biometric') ?? false;

  UserModel get user => UserModel.fromJson(_storage.read('user') ?? {});
  set user(UserModel userModel) => _storage.write('user', userModel.toJson());

  set users(List<dynamic> users) => _storage.write('users', users);
  List<dynamic> get users => _storage.read('users') ?? <dynamic>[];

  List<UserModel> get usersModel {
    final _users = <UserModel>[];
    for (var item in users) {
      _users.add(UserModel.fromJson(item));
    }
    return _users;
  }

  // Lista de favoritos
  set favorites(List<dynamic> favorites) => _storage.write('favorites', favorites);
  List<dynamic> get favorites => _storage.read('favorites') ?? <dynamic>[];

  List<FavoriteModel> get favoritesModel {
    final _favorites = <FavoriteModel>[];
    for (var item in favorites) {
      _favorites.add(FavoriteModel.fromJson(item));
    }
    return _favorites;
  }

  // Lista de productos
  set products(List<dynamic> products) => _storage.write('products', products);
  List<dynamic> get products => _storage.read('products') ?? <dynamic>[];

  List<ProductModel> get productsModel {
    final _products = <ProductModel>[];
    for (var item in products) {
      _products.add(ProductModel.fromJson(item));
    }
    return _products;
  }

  // Carrito de compra
  set karts(List<dynamic> karts) => _storage.write('karts', karts);
  List<dynamic> get karts => _storage.read('karts') ?? <dynamic>[];

  List<KartModel> get kartsModel {
    final _karts = <KartModel>[];
    for (var item in karts) {
      _karts.add(KartModel.fromJson(item));
    }
    return _karts;
  }
}
