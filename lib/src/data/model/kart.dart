import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/user.dart';

class KartModel {
  KartModel({
    required this.cantidad,
    required this.price,
    required this.product,
    required this.user,
  });

  int cantidad;
  double price;
  ProductModel product;
  UserModel user;

  factory KartModel.fromJson(Map<String, dynamic> json) => KartModel(
        cantidad: json["cantidad"] ?? 0,
        price: json["price"] ?? 0.0,
        product: ProductModel.fromJson(json["product"] ?? {}),
        user: UserModel.fromJson(json["user"] ?? {}),
      );

  Map<String, dynamic> toJson() => {
        "cantidad": cantidad,
        "price": price,
        "product": product.toJson(),
        "user": user.toJson(),
      };
}
