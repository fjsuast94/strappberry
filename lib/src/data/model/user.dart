class UserModel {
  UserModel({
    required this.id,
    required this.uid,
    required this.name,
    required this.password,
    required this.email,
    required this.photo,
    required this.signIn,
    required this.rol,
  });

  int id;
  String uid;
  String name;
  String password;
  String email;
  String photo;
  String rol;
  int signIn;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"] ?? 0,
        uid: json["uid"] ?? "",
        name: json["name"] ?? "",
        password: json["password"] ?? "",
        email: json["email"] ?? "",
        photo: json["photo"] ?? "https://autocinemaimperial.com/images/avatar-masculino.png",
        rol: json["rol"] ?? 'Cliente',
        signIn: json["sign_in_with"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uid": uid,
        "name": name,
        "password": password,
        "email": email,
        "photo": photo,
        "rol": rol,
        "sign_in_with": signIn,
      };

  /*  @override
  String toString() {
    //print("Usermodel ${toJson()}");
    return super.toString();
  } */
}
