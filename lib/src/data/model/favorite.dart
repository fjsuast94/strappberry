import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/user.dart';

class FavoriteModel {
  FavoriteModel({
    required this.product,
    required this.user,
  });

  ProductModel product;
  UserModel user;

  factory FavoriteModel.fromJson(Map<String, dynamic> json) => FavoriteModel(
        product: ProductModel.fromJson(json["product"] ?? {}),
        user: UserModel.fromJson(json["user"] ?? {}),
      );

  Map<String, dynamic> toJson() => {
        "product": product.toJson(),
        "user": user.toJson(),
      };
}
