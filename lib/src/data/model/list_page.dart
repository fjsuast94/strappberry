class ListPage<ItemType> {
  ListPage({
    required this.totalCount,
    required this.data,
    this.message,
  });

  final int totalCount;
  final List<ItemType> data;
  final String? message;

  bool isLastPage(int previouslyFetchedItemsCount) {
    final newItemsCount = data.length;
    final totalFetchedItemsCount = previouslyFetchedItemsCount + newItemsCount;
    return totalFetchedItemsCount == totalCount;
  }
}
