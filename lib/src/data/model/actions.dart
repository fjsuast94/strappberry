import 'package:flutter/material.dart';

class ActionsModel {
  String title;
  String page;
  Color? backGroundColor;
  Icon icon;

  ActionsModel({
    required this.title,
    this.backGroundColor,
    required this.icon,
    required this.page,
  });

  factory ActionsModel.fromJson(Map<String, dynamic> json) => ActionsModel(
        title: json["title"],
        page: json["page"],
        icon: json["icon"],
        backGroundColor: json["backGroundColor"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "icon": icon,
        "page": page,
        "backGroundColor": backGroundColor,
      };
}
