class ProductModel {
  ProductModel({
    required this.uuid,
    required this.name,
    required this.price,
    required this.description,
    required this.image,
    required this.favorite,
  });

  String uuid;
  String name;
  double price;
  String description;
  String image;
  bool favorite;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        uuid: json["uuid"] ?? "",
        name: json["name"] ?? "",
        price: json["price"] ?? 0.0,
        description: json["description"] ?? "",
        image: json["image"] ?? "",
        favorite: json["favorite"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "price": price,
        "description": description,
        "image": image,
        "favorite": favorite,
      };
}
