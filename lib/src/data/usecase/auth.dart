import 'package:flutter/material.dart';
import 'package:strappberry/src/data/model/response.dart';
import 'package:strappberry/src/data/model/user.dart';
import 'package:strappberry/src/data/repository/local/http/http_dio.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';

class AuthCase {
  AuthCase({
    required HttpDio httpDio,
    required StorageRepository storage,
  })  : _httpDio = httpDio,
        _storage = storage;

  final HttpDio _httpDio;
  final StorageRepository _storage;

  Future<void> loadUsers() async {
    try {
      final users = _storage.users;
      if (users.isEmpty) {
        Map<String, dynamic> json = {
          "name": "Admin",
          "email": "admin@gmail.com",
          "password": "admin",
          "rol": "Admin",
        };
        users.add(json);
        _storage.users = users;
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<UserModel> getUser() async {
    return _storage.user;
  }

  Future<void> logOut() async {
    _storage.user = UserModel.fromJson({});
  }

  Future<ResponseModel> login(String email, String password) async {
    final response = ResponseModel(status: false);
    try {
      final users = _storage.usersModel;
      final user = users.firstWhere((e) => e.email == email && e.password == password);
      _storage.user = user;
      response.data = user.rol;
      response.status = true;
    } catch (e) {
      debugPrint(e.toString());
      response.message = "Usuario o contraseña es incorrecta";
    }

    return response;
  }

  Future<ResponseModel> register(String name, String email, String password) async {
    final response = ResponseModel(status: false);
    try {
      final users = _storage.users;
      Map<String, dynamic> json = {
        "name": name,
        "email": email,
        "password": password,
      };
      users.add(json);
      _storage.users = users;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }
}
