import 'package:strappberry/src/data/model/kart.dart';
import 'package:strappberry/src/data/model/list_page.dart';
import 'package:strappberry/src/data/model/response.dart';
import 'package:collection/collection.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';

class KartUseCase {
  KartUseCase({
    required StorageRepository storage,
  }) : _storage = storage;

  final StorageRepository _storage;

  Future<void> clearKarts() async {
    _storage.karts.clear();
  }

  Future<ListPage<KartModel>> getKarts() async {
    List<KartModel> data = <KartModel>[];
    String message = "";
    try {
      final user = _storage.user;
      data = _storage.kartsModel.where((e) => e.user.email == user.email).toList();
    } catch (e) {
      message = e.toString();
    }
    return ListPage<KartModel>(
      data: data,
      totalCount: data.length,
      message: message,
    );
  }

  Future<ResponseModel> createkarts(KartModel kart) async {
    final response = ResponseModel(status: false);
    try {
      final _kart =
          _storage.kartsModel.firstWhereOrNull((e) => e.product.name == kart.product.name);
      final karts = _storage.karts;
      if (_kart == null) {
        karts.add(kart.toJson());
      } else {
        _kart.cantidad = _kart.cantidad + 1;
        int index = _storage.kartsModel.indexWhere((e) => e.product.name == _kart.product.name);
        karts.removeAt(index);
        karts.insert(index, _kart.toJson());
      }
      _storage.karts = karts;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> updatekarts(int index, KartModel kart) async {
    final response = ResponseModel(status: false);
    try {
      final karts = _storage.karts;
      karts[index] = kart.toJson();
      _storage.karts = karts;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> deletekarts(int index) async {
    final response = ResponseModel(status: false);
    try {
      final karts = _storage.karts;
      karts.removeAt(index);
      _storage.karts = karts;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }
}
