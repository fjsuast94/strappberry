import 'package:strappberry/src/data/model/list_page.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/response.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';

class ProductUseCase {
  ProductUseCase({
    required StorageRepository storage,
  }) : _storage = storage;

  final StorageRepository _storage;

  Future<ListPage<ProductModel>> getProducts() async {
    List<ProductModel> data = <ProductModel>[];
    String message = "";
    try {
      data = _storage.productsModel;
    } catch (e) {
      message = e.toString();
    }
    return ListPage<ProductModel>(
      data: data,
      totalCount: data.length,
      message: message,
    );
  }

  Future<ResponseModel> createProducts(ProductModel product) async {
    final response = ResponseModel(status: false);
    try {
      final products = _storage.products;
      products.add(product.toJson());
      _storage.products = products;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> updateProduct(ProductModel product) async {
    final response = ResponseModel(status: false);
    try {
      final products = _storage.products;
      int index = _storage.productsModel.indexWhere((e) => e.uuid == product.uuid);
      if (index >= 0) {
        products.removeAt(index);
        products.insert(index, product.toJson());
      }
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> updateProducts(int index, ProductModel product) async {
    final response = ResponseModel(status: false);
    try {
      final products = _storage.products;
      products[index] = product;
      _storage.products = products;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> deleteProducts(int index) async {
    final response = ResponseModel(status: false);
    try {
      final products = _storage.products;
      products.removeAt(index);
      _storage.products = products;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }
}
