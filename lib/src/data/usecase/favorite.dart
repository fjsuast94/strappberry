import 'package:strappberry/src/data/model/favorite.dart';
import 'package:strappberry/src/data/model/list_page.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/response.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';

class FavoritetUseCase {
  FavoritetUseCase({
    required StorageRepository storage,
  }) : _storage = storage;

  final StorageRepository _storage;

  Future<ListPage<FavoriteModel>> getFavorites() async {
    List<FavoriteModel> data = <FavoriteModel>[];
    String message = "";
    final user = _storage.user;

    try {
      final _data = _storage.favoritesModel
          .where((e) => e.product.favorite && e.user.email == user.email)
          .toList();
      data = _data;
    } catch (e) {
      message = e.toString();
    }
    return ListPage<FavoriteModel>(
      data: data,
      totalCount: data.length,
      message: message,
    );
  }

  Future<ResponseModel> createFavorite(ProductModel product) async {
    final response = ResponseModel(status: false);
    final user = _storage.user;
    try {
      final favorites = _storage.favorites;

      favorites.add(FavoriteModel.fromJson({
        "product": product.toJson(),
        "user": user.toJson(),
      }).toJson());
      _storage.favorites = favorites;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> updateFavorite(int index, ProductModel product) async {
    final response = ResponseModel(status: false);
    try {
      final favorites = _storage.favorites;
      favorites[index] = product;
      _storage.favorites = favorites;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }

  Future<ResponseModel> deleteFavorite(int index) async {
    final response = ResponseModel(status: false);
    try {
      final favorites = _storage.favorites;
      favorites.removeAt(index);
      _storage.favorites = favorites;
      response.status = true;
    } catch (e) {
      response.message = e.toString();
    }
    return response;
  }
}
