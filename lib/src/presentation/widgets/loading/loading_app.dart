import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoadingApp extends StatelessWidget {
  static void show(BuildContext context, {Key? key}) => Get.dialog(
        LoadingApp(key: key),
        barrierDismissible: false,
      ).then((value) => FocusScope.of(context).requestFocus(FocusNode()));
  static void hide(BuildContext context) => Get.back();

  const LoadingApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Center(
        child: Card(
          shadowColor: Colors.transparent,
          elevation: 0.0,
          color: Colors.transparent,
          child: Container(
            width: 80,
            height: 80,
            padding: const EdgeInsets.all(12.0),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
