import 'package:flutter/material.dart';

class RefreshScrollView extends StatelessWidget {
  final Widget child;
  final VoidCallback? onRefresh;
  final ScrollController? controller;
  final bool reverse;
  final ScrollPhysics? physics;

  const RefreshScrollView({
    Key? key,
    required this.child,
    this.onRefresh,
    this.controller,
    this.reverse = false,
    this.physics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      strokeWidth: 1.0,
      onRefresh: () => Future.sync(onRefresh ?? () {}),
      child: CustomScrollView(
        physics: physics,
        reverse: reverse,
        controller: controller,
        slivers: [
          SliverToBoxAdapter(child: child),
        ],
      ),
    );
  }
}
