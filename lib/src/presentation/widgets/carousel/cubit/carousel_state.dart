part of 'carousel_cubit.dart';

class CarouselState {
  CarouselState({
    this.currentIndex = 0,
  });

  final int currentIndex;
}
