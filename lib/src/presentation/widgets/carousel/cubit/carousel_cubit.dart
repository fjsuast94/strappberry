import 'package:bloc/bloc.dart';

part 'carousel_state.dart';

class CarouselCubit extends Cubit<CarouselState> {
  CarouselCubit() : super(CarouselState());

  void selectIndex(int index) {
    emit(CarouselState(currentIndex: index));
  }
}
