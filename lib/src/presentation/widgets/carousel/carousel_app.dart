import 'package:strappberry/src/presentation/widgets/carousel/cubit/carousel_cubit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarouselApp extends StatelessWidget {
  const CarouselApp({
    Key? key,
    required this.listImage,
    this.network = true,
    this.fit = BoxFit.cover,
  }) : super(key: key);

  final List<String> listImage;
  final bool network;
  final BoxFit fit;

  @override
  Widget build(BuildContext context) {
    final carouselController = CarouselController();
    // final size = MediaQuery.of(context).size;
    final List<Widget> imageSliders = listImage
        .map(
          (item) => Container(
            child: network
                ? CachedNetworkImage(
                    imageUrl: item,
                    imageBuilder: (context, image) => Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: const Color(0xFFAABBCC),
                        image: DecorationImage(
                          image: image,
                          fit: fit,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        color: Color(0xFFAABBCC),
                        /* image: DecorationImage(
                          image: Image.asset("assets/images/cargando-imagen.png").image,
                          fit: BoxFit.contain,
                        ), */
                      ),
                    ),
                    errorWidget: (context, url, error) => Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        color: Color(0xFFAABBCC),
                        /* image: DecorationImage(
                          image: Image.asset("assets/images/imagen-no-disponible.png").image,
                          fit: BoxFit.contain,
                        ), */
                      ),
                    ),
                    fadeOutDuration: const Duration(milliseconds: 400),
                    fadeInDuration: const Duration(milliseconds: 800),
                  )
                : Image.asset(
                    item,
                    fit: fit,
                    width: double.infinity,
                  ),
          ),
        )
        .toList();

    return BlocProvider(
      create: (context) => CarouselCubit(),
      child: BlocBuilder<CarouselCubit, CarouselState>(
        builder: (context, state) {
          final bloc = context.read<CarouselCubit>();
          return Stack(
            alignment: Alignment.bottomCenter,
            children: [
              CarouselSlider(
                carouselController: carouselController,
                items: imageSliders,
                options: CarouselOptions(
                  autoPlay: listImage.length > 1 ? true : false,
                  enableInfiniteScroll: listImage.length > 1 ? true : false,
                  enlargeCenterPage: true,
                  aspectRatio: 20 / 9,
                  viewportFraction: 1.0,
                  autoPlayCurve: Curves.easeOut,
                  onPageChanged: (index, reason) => bloc.selectIndex(index),
                ),
              ),
              Positioned(
                bottom: 0.0,
                left: 10.0,
                child: listImage.length > 1
                    ? AnimatedSwitcher(
                        duration: const Duration(milliseconds: 500),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: List.generate(listImage.length, (index) {
                            return Container(
                              width: 10.0,
                              height: 10.0,
                              margin: const EdgeInsets.symmetric(
                                vertical: 5.0,
                                horizontal: 2.0,
                              ),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: state.currentIndex == index
                                    ? Colors.white.withOpacity(0.8)
                                    : Colors.white.withOpacity(0.1),
                              ),
                            );
                          }),
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          );
        },
      ),
    );
  }
}
