import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrWidget extends StatelessWidget {
  const QrWidget({
    Key? key,
    required this.value,
    this.size = 350.0,
  }) : super(key: key);

  final String value;
  final double size;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(
            20,
          ),
        ),
      ),
      alignment: Alignment.center,
      child: QrImage(
        backgroundColor: Colors.white,
        data: value,
        size: size,
      ),
    );
  }
}
