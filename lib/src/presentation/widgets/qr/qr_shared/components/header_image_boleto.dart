import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/cubit/qr_shared_cubit.dart';

class HeaderImageBoleto extends StatelessWidget {
  const HeaderImageBoleto({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QrSharedCubit, QrSharedState>(
      builder: (context, state) {
        return Container(
          width: double.infinity,
          height: state.headerHeight * 0.4,
          decoration: BoxDecoration(
            color: AppColors.kPrimaryColor,
            image: DecorationImage(
              image: Image.asset(
                "assets/images/logos/logo_strappberry.png",
              ).image,
            ),
            borderRadius: const BorderRadius.vertical(
              top: Radius.circular(20),
            ),
          ),
        );
      },
    );
  }
}
