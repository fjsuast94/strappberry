import 'package:flutter/material.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';

class InstruccionesRecomendacionesBoleto extends StatelessWidget {
  const InstruccionesRecomendacionesBoleto({
    Key? key,
    this.recomendacion = "",
    this.terminos = "",
  }) : super(key: key);

  final String recomendacion;
  final String terminos;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Column(
      children: [
        Text(
          "Instrucciones de uso y recomendaciones",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: AppColors.kPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: responsive.inchPercent(1.6),
          ),
        ),
        Text(
          recomendacion,
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: responsive.inchPercent(1.5),
            color: Colors.black.withOpacity(0.7),
          ),
        ),
        const SizedBox(height: 20),
        Text(
          terminos,
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: responsive.inchPercent(1.5),
            color: Colors.black.withOpacity(0.7),
          ),
        ),
      ],
    );
  }
}
