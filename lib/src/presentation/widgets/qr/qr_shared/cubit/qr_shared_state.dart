part of 'qr_shared_cubit.dart';

class QrSharedState {
  QrSharedState({
    this.width = 0.0,
    this.height = 0.0,
    this.headerHeight = 0.0,
    this.storage,
  });
  final StorageRepository? storage;
  final double width;
  final double height;
  final double headerHeight;

  QrSharedState copyWith({
    StorageRepository? storage,
    double? width,
    double? height,
    double? headerHeight,
  }) =>
      QrSharedState(
        storage: storage ?? this.storage,
        width: width ?? this.width,
        height: height ?? this.height,
        headerHeight: headerHeight ?? this.headerHeight,
      );
}
