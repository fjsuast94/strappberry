import 'package:bloc/bloc.dart';

import 'package:screenshot/screenshot.dart';
import 'package:share_extend/share_extend.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';

part 'qr_shared_state.dart';

class QrSharedCubit extends Cubit<QrSharedState> {
  QrSharedCubit({
    required StorageRepository storage,
    required ScreenshotController screenshotController,
    required Responsive responsive,
  })  : _storage = storage,
        _screenshotController = screenshotController,
        _responsive = responsive,
        super(QrSharedState());

  final StorageRepository _storage;
  final Responsive _responsive;
  final ScreenshotController _screenshotController;
  ScreenshotController get screenshotController => _screenshotController;

  Future<void> init() async {
    double width = (_responsive.width - _responsive.inchPercent(6)).floorToDouble();
    print(width);
    double height = (_responsive.height / 1.5).floorToDouble();
    double headerHeight = ((width - _responsive.inchPercent(4)) / 2).floorToDouble();
    emit(
      state.copyWith(
        storage: _storage,
        width: width,
        height: height,
        headerHeight: headerHeight,
      ),
    );
  }

  Future<void> shareTapped(String folio) async {
    /* try {
      final file = await _screenshotController.capture(
        pixelRatio: 1.5,
      );
      ShareExtend.share(
        file.path,
        "image",
        extraText: "Cupon Qr $folio",
      );
    } catch (e) {
      print(e);
    } */
  }
}
