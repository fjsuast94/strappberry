import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:screenshot/screenshot.dart';
import 'package:get/get_utils/get_utils.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/components/header_image_boleto.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/components/instrucciones_recomedaciones_boleto.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/components/qr_boleto.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/cubit/qr_shared_cubit.dart';

class ShareQrCard extends StatelessWidget {
  final QrCardModel qrValue;

  const ShareQrCard({
    Key? key,
    required this.qrValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => QrSharedCubit(
        screenshotController: ScreenshotController(),
        storage: context.read(),
        responsive: responsive,
      )..init(),
      child: BlocConsumer<QrSharedCubit, QrSharedState>(
        listener: (context, state) {},
        builder: (context, state) {
          final bloc = context.read<QrSharedCubit>();
          return Screenshot(
            controller: bloc.screenshotController,
            child: SimpleDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              backgroundColor: Colors.white,
              titlePadding: EdgeInsets.zero,
              title: const HeaderImageBoleto(),
              children: [
                Container(
                  width: state.width,
                  height: state.height * 0.7,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: state.height * 0.6,
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              QrWidget(
                                value: qrValue.boletoQr!,
                                size: responsive.height * 0.3,
                              ),
                              InstruccionesRecomendacionesBoleto(
                                recomendacion: qrValue.recomendacion!,
                                terminos: qrValue.terminos!,
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () => bloc.shareTapped(qrValue.boletoQr!),
                          child: SizedBox(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'share'.tr,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: responsive.inchPercent(1.5),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const Icon(Icons.share),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class QrCardModel {
  double? tarifaVehicularPrecio;
  double? tarifaExtraPrecio;
  String? fecha;
  String? titular;
  int? numVehiculos;
  int? numPersonas;
  double? total;
  String? boletoQr;
  String? terminos;
  String? recomendacion;

  QrCardModel({
    this.tarifaVehicularPrecio,
    this.tarifaExtraPrecio,
    this.fecha,
    this.titular,
    this.numPersonas,
    this.numVehiculos,
    this.total,
    this.boletoQr,
    this.recomendacion,
    this.terminos,
  });
}
