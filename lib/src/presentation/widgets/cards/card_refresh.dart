import 'package:flutter/material.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';

class CardRefresh extends StatelessWidget {
  final Widget? title;
  final Widget? content;
  final double? margin;

  const CardRefresh({
    Key? key,
    this.title = const SizedBox.shrink(),
    this.margin,
    this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: responsive.inchPercent(0.5),
      ),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            title!,
            Column(
              children: const [
                Text("Tire para refrescar"),
                Icon(Icons.arrow_downward),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
