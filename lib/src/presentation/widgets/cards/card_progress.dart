import 'package:flutter/material.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';

class CardProgress extends StatelessWidget {
  const CardProgress({
    Key? key,
    required this.title,
    required this.value,
    this.color = AppColors.kPrimaryColor,
  }) : super(key: key);

  final String title;
  final Color? color;
  final double value;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          child: SizedBox(
            width: 180,
            child: LinearProgressIndicator(
              backgroundColor: Colors.grey[400],
              color: color,
              value: value,
              minHeight: 5,
            ),
          ),
        ),
      ],
    );
  }
}
