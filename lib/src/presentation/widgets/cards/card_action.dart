import 'package:flutter/material.dart';
import 'package:strappberry/src/data/model/actions.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';

class CardAction extends StatelessWidget {
  const CardAction({
    Key? key,
    required this.action,
    this.onTap,
  }) : super(key: key);

  final ActionsModel action;
  final VoidCallback? onTap;
  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Tooltip(
      message: action.title,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: 89,
          height: 100,
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                blurRadius: 7.0,
                color: Colors.grey.withOpacity(0.3),
              ),
            ],
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  action.icon,
                  const SizedBox(height: 10),
                  Text(
                    action.title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: responsive.inchPercent(1.5),
                      color: Colors.black.withOpacity(0.5),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
