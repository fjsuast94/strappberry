export 'package:dropdown_search/dropdown_search.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
// ignore: implementation_imports
import 'package:flutter_form_bloc/src/utils/utils.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';

class DropdownBloc<Value> extends StatefulWidget {
  const DropdownBloc({
    Key? key,
    this.label = '',
    required this.selectFieldBloc,
    required this.itemBuilder,
    this.onChanged,
    this.nextFocusNode,
    this.decoration = const InputDecoration(),
    this.errorBuilder,
    this.animateWhenCanShow = true,
    this.margin,
    this.pading = const EdgeInsets.symmetric(
      horizontal: 10,
    ),
    this.mode = Mode.MENU,
    this.backGround = Colors.white,
    this.prefixIcon,
    this.dropDownButton = const Icon(
      Icons.arrow_downward_outlined,
      size: 18,
    ),
    this.popupItemDisabled,
    this.enabled = true,
  }) : super(key: key);

  final String label;
  final bool enabled;
  final SelectFieldBloc<Value, dynamic> selectFieldBloc;
  final FieldBlocStringItemBuilder<String> itemBuilder;
  final FocusNode? nextFocusNode;
  final Function(Value)? onChanged;
  final bool animateWhenCanShow;
  final InputDecoration decoration;
  final FieldBlocErrorBuilder? errorBuilder;
  final Mode mode;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry pading;
  final Color backGround;
  final Widget? prefixIcon;
  final Widget? dropDownButton;
  final DropdownSearchPopupItemEnabled<Value>? popupItemDisabled;

  @override
  _DropdownBlocState<Value> createState() => _DropdownBlocState<Value>();
}

class _DropdownBlocState<Value> extends State<DropdownBloc<Value>> {
  @override
  Widget build(BuildContext context) {
    return CanShowFieldBlocBuilder(
      fieldBloc: widget.selectFieldBloc,
      animate: widget.animateWhenCanShow,
      builder: (_, __) {
        return BlocBuilder<SelectFieldBloc<Value, dynamic>, SelectFieldBlocState<Value, dynamic>>(
          bloc: widget.selectFieldBloc,
          builder: (context, fieldState) {
            final isEnabled = fieldBlocIsEnabled(
              isEnabled: true,
              enableOnlyWhenFormBlocCanSubmit: false,
              fieldBlocState: fieldState,
            );
            final errorText = Style.getErrorText(
              context: context,
              errorBuilder: widget.errorBuilder,
              fieldBlocState: fieldState,
              fieldBloc: widget.selectFieldBloc,
            );
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.label,
                  style: const TextStyle(
                    fontSize: 12.0,
                    color: AppColors.kPrimaryColor,
                  ),
                ),
                DropdownSearch<Value>(
                  enabled: widget.enabled,
                  mode: widget.mode,
                  popupBackgroundColor: Colors.white,
                  dropDownButton: widget.dropDownButton,
                  dropdownSearchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.zero,
                    prefixIcon: widget.prefixIcon,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffE4E6E8), width: 1.0),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffE4E6E8), width: 1.0),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffE4E6E8), width: 1.0),
                    ),
                    labelStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey[600],
                    ),
                  ),
                  // ignore: deprecated_member_use
                  //label: widget.label,
                  showSelectedItems: true,
                  items: fieldState.items,
                  popupItemDisabled: widget.popupItemDisabled,
                  onChanged: fieldBlocBuilderOnChange<Value?>(
                    isEnabled: isEnabled,
                    nextFocusNode: widget.nextFocusNode,
                    onChanged: (value) {
                      widget.selectFieldBloc.updateValue(value);
                      if (widget.onChanged != null) {
                        widget.onChanged!(value!);
                      }
                      FocusScope.of(context).requestFocus(FocusNode());
                    },
                  ),
                  dropdownBuilder: (context, selectedItem) => Text(
                    selectedItem == null ? "" : selectedItem.toString(),
                  ),
                  selectedItem: fieldState.value,
                  validator: (validate) => errorText,
                ),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 500),
                  child: errorText == null || errorText.isEmpty
                      ? const SizedBox.shrink()
                      : Text(
                          errorText,
                          style: const TextStyle(
                            color: Colors.red,
                            fontSize: 12,
                          ),
                        ),
                )
              ],
            );
          },
        );
      },
    );
  }
}
