import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NumericStepButton extends StatelessWidget {
  final int minValue;
  final int maxValue;
  final int total;
  final ValueChanged<int>? onChanged;
  final Color? iconButtonColor;
  final double? iconSize, splashRadius;
  final TextStyle? counterStyle;
  final MainAxisAlignment mainAxisAlignment;
  final Text? title;
  final BorderRadiusGeometry? borderRadius;

  const NumericStepButton({
    Key? key,
    this.minValue = 1,
    this.maxValue = 10,
    this.total = 1,
    this.onChanged,
    this.iconButtonColor,
    this.iconSize = 20.0,
    this.counterStyle,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.splashRadius = 30,
    this.title,
    this.borderRadius,
  }) : super(key: key);

  /*  borderRadius : BorderRadius.all(
                  const Radius.circular(
                    20.0,
                  ), */

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CounterBloc(
        maxValue: maxValue,
        minValue: minValue,
        total: total,
      ),
      child: BlocConsumer<CounterBloc, int>(
        listener: (context, state) {
          if (onChanged != null) {
            onChanged!(state);
          }
        },
        builder: (context, state) {
          final bloc = context.read<CounterBloc>();
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AnimatedContainer(
                  duration: const Duration(milliseconds: 500),
                  child: title != null
                      ? Container(
                          margin: const EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          child: title,
                        )
                      : const SizedBox.shrink(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        if (state > bloc.minValue) {
                          bloc.add(Decrement());
                        }
                      },
                      child: Icon(
                        Icons.remove,
                        color: Colors.black,
                        size: iconSize,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      state.toString(),
                      textAlign: TextAlign.center,
                      style: counterStyle ??
                          const TextStyle(
                            color: Colors.black87,
                            fontSize: 12.0,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        if (state < bloc.maxValue) {
                          bloc.add(Increment());
                        }
                      },
                      child: Icon(
                        Icons.add,
                        color: Colors.black,
                        size: iconSize,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

abstract class CounterEvent {}

class Increment extends CounterEvent {}

class Decrement extends CounterEvent {}

class CounterBloc extends Bloc<CounterEvent, int> {
  final int minValue;
  final int maxValue;
  final int total;

  CounterBloc({
    this.minValue = 1,
    this.maxValue = 20,
    this.total = 0,
  }) : super(total) {
    on<Increment>((event, emit) => emit(state + 1));
    on<Decrement>((event, emit) => emit(state - 1));
  }
}
