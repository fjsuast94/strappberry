import 'package:flutter/material.dart';

class BottomNavigationButtom extends StatelessWidget {
  const BottomNavigationButtom({
    Key? key,
    required this.titleNext,
    this.titleBack,
    this.onNext,
    this.onBack,
  }) : super(key: key);

  final VoidCallback? onNext;
  final VoidCallback? onBack;

  final Widget? titleNext;
  final Widget? titleBack;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.0,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 800),
        child: titleBack != null
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SizedBox(
                      height: 60.0,
                      child: ClipRRect(
                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                        child: TextButton(
                          onPressed: onBack,
                          child: titleBack!,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                      child: SizedBox(
                    height: 60.0,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      child: TextButton(
                        onPressed: onNext,
                        child: titleNext!,
                      ),
                    ),
                  ))
                ],
              )
            : SizedBox(
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  child: TextButton(
                    onPressed: onNext,
                    child: titleNext!,
                  ),
                ),
              ),
      ),
    );
  }
}
