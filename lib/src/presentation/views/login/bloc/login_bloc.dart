import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/presentation/core/utils/validator_string.dart';

enum ResponseStatus { admin, client }

class LoginBloc extends FormBloc<String, String> {
  LoginBloc({
    required AuthCase auth,
  }) : _auth = auth {
    addStatus(ResponseStatus.client);
    addFieldBlocs(fieldBlocs: [email, password]);
  }

  final AuthCase _auth;

  final email = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.emailFormat,
    ],
  );
  final password = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  final BehaviorSubject<ResponseStatus> _status = BehaviorSubject<ResponseStatus>();
  Function(ResponseStatus) get addStatus => _status.sink.add;
  Stream<ResponseStatus> get getStatus => _status.stream;
  ResponseStatus get status => _status.value;

  @override
  void onSubmitting() async {
    await Future.delayed(const Duration(milliseconds: 1000));
    final response = await _auth.login(email.value!, password.value!);
    if (response.status!) {
      addStatus(response.data == "Admin" ? ResponseStatus.admin : ResponseStatus.client);
      emitSuccess(canSubmitAgain: true);
    } else {
      emitFailure(failureResponse: response.message);
    }
  }
}
