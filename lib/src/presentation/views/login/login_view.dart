import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/views/login/bloc/login_bloc.dart';
import 'package:strappberry/src/presentation/widgets/form-bloc/input_text_cupertino.dart';
import 'package:strappberry/src/presentation/widgets/form-bloc/un_focus_form.dart';
import 'package:strappberry/src/presentation/widgets/loading/loading_app.dart';

class LoginView extends StatelessWidget {
  static const String routeName = "/login";
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => LoginBloc(
        auth: context.read(),
      ),
      child: Builder(
        builder: (context) {
          final bloc = context.read<LoginBloc>();
          return FormBlocListener<LoginBloc, String, String>(
            onSubmitting: (context, state) => LoadingApp.show(context),
            onSuccess: (context, state) {
              LoadingApp.hide(context);
              Routes.offAndToNamed(bloc.status == ResponseStatus.admin ? "/products" : "/navbar");
            },
            onFailure: (context, state) {
              LoadingApp.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: Scaffold(
              body: UnFocusForm(
                child: SingleChildScrollView(
                  child: Container(
                    height: responsive.height,
                    decoration: const BoxDecoration(
                      color: AppColors.kBackGroundSecondary,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 80),
                        Center(
                          child: Image(
                            width: 250,
                            image: Image.asset(
                              "assets/images/logos/logo_strappberry.png",
                            ).image,
                          ),
                        ),
                        const SizedBox(height: 70),
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            height: responsive.height,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                              child: Column(
                                children: <Widget>[
                                  const SizedBox(height: 40),
                                  InputTextCupertino(
                                    textFieldBloc: bloc.email,
                                    placeholder: 'Email',
                                    keyboardType: TextInputType.emailAddress,
                                  ),
                                  const SizedBox(height: 10),
                                  InputTextCupertino(
                                    textFieldBloc: bloc.password,
                                    placeholder: 'Contraseña',
                                    keyboardType: TextInputType.emailAddress,
                                    suffixButton: SuffixButton.obscureText,
                                  ),
                                  const SizedBox(height: 10),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(14),
                                      child: SizedBox(
                                        width: 150,
                                        child: TextButton(
                                          onPressed: bloc.submit,
                                          child: const Text("Ingresar"),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        const Text("¿Aun no tienes cuenta?"),
                                        GestureDetector(
                                          onTap: () => Routes.offAndToNamed("/register"),
                                          child: const Text(
                                            "Registrate",
                                            style: TextStyle(color: AppColors.kPrimaryColor),
                                          ),
                                        ),
                                        const Text(
                                          "Tu nombre | micorreo@gmail.com",
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
