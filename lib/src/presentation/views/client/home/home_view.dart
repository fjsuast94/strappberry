import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/views/client/home/components/home_app_bar.dart';
import 'package:strappberry/src/presentation/views/client/home/components/home_body.dart';
import 'package:strappberry/src/presentation/views/client/home/cubit/home_cubit.dart';

class HomeView extends StatelessWidget {
  static const String routeName = "/client-home";
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit(
        auth: context.read(),
        productUse: context.read(),
      )..init(),
      child: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) {
          return Scaffold(
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: state.loading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          HomeAppBar(),
                          Expanded(
                            child: HomeBody(),
                          ),
                        ],
                      ),
              ),
            ),
          );
        },
      ),
    );
  }
}
