part of 'home_cubit.dart';

class HomeState {
  HomeState({
    this.loading = false,
    this.user,
    this.product,
    this.products = const <ProductModel>[],
  });

  bool loading;
  UserModel? user;
  ProductModel? product;
  List<ProductModel> products;

  HomeState copyWith({
    bool? loading,
    UserModel? user,
    ProductModel? product,
    List<ProductModel>? products,
  }) =>
      HomeState(
        loading: loading ?? this.loading,
        user: user ?? this.user,
        product: product ?? this.product,
        products: products ?? this.products,
      );
}
