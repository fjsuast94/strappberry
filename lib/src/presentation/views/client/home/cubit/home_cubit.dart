import 'package:bloc/bloc.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/model/user.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/data/usecase/product.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({
    required AuthCase auth,
    required ProductUseCase productUse,
  })  : _auth = auth,
        _productUse = productUse,
        super(HomeState());

  final AuthCase _auth;
  final ProductUseCase _productUse;

  void init() async {
    emit(state.copyWith(loading: true));
    final products = await _productUse.getProducts();
    emit(state.copyWith(user: await _auth.getUser(), products: products.data, loading: false));
  }

  void setProduct(ProductModel _product) {
    emit(state.copyWith(product: _product));
  }
}
