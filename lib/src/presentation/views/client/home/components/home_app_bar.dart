import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/bloc/kart/kart_cubit.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/views/client/home/cubit/home_cubit.dart';

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    final cubit = context.read<HomeCubit>();
    final _kartCubit = context.watch<KartCubit>();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () => Routes.goToName("/client-kart"),
              behavior: HitTestBehavior.translucent,
              child: Badge(
                badgeContent: Text(
                  _kartCubit.state.karts.length.toString(),
                  style: const TextStyle(color: Colors.white, fontSize: 10),
                ),
                position: BadgePosition.topStart(top: -10, start: 18),
                badgeColor: AppColors.kBackGroundSecondary,
                child: const Icon(
                  Icons.shopping_cart_outlined,
                  size: 25,
                ),
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Hola ${cubit.state.user!.name}",
              style: TextStyle(
                fontSize: responsive.inchPercent(2.2),
                color: Colors.black45,
              ),
            ),
            const SizedBox(height: 5),
            Text(
              "¿Vamos a comprar algo?",
              style: TextStyle(
                fontSize: responsive.inchPercent(2),
                color: Colors.black.withOpacity(0.7),
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
