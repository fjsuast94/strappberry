import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';

class HomeBottomNavigationBar extends StatelessWidget {
  const HomeBottomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(30),
        topRight: Radius.circular(30),
      ),
      child: CustomNavigationBar(
        backgroundColor: AppColors.kBackGround,
        items: [
          CustomNavigationBarItem(
            icon: Image(
              image: Image.asset("assets/images/iconos/home.png").image,
            ),
          ),
          CustomNavigationBarItem(
            icon: Image(
              image: Image.asset("assets/images/iconos/heart.png").image,
            ),
          ),
          CustomNavigationBarItem(
            icon: Image(
              image: Image.asset("assets/images/iconos/person.png").image,
            ),
          ),
        ],
      ),
    );
  }
}
