import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/bloc/favorite/favorite_cubit.dart';
import 'package:strappberry/src/presentation/bloc/kart/kart_cubit.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/views/client/home/cubit/home_cubit.dart';

class ProductView extends StatelessWidget {
  static const String routeName = "/client-product";
  const ProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<HomeCubit>();
    final _kartCubit = context.watch<KartCubit>();
    final _favorite = context.watch<FavoriteCubit>();

    final _product = cubit.state.product;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.kBackGround,
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {
              _product!.favorite = true;
              cubit.setProduct(_product);
              _favorite.addFavorite(_product);
            },
            icon: Icon(
              Icons.favorite,
              color: _product!.favorite ? AppColors.kCancel : AppColors.kIconColor,
            ),
          ),
        ],
        iconTheme: const IconThemeData(
          color: AppColors.kIconColor,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 250,
            width: double.infinity,
            decoration: const BoxDecoration(
              color: AppColors.kBackGround,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 220,
                  width: 300,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: FileImage(File(_product.image)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  child: Text(
                    _product.name,
                    style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  child: Text(
                    Helper.moneyFormat(_product.price),
                    style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  child: Text(
                    _product.description,
                    style: const TextStyle(
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: TextButton(
            onPressed: () {
              Helper.success(message: "Se ha agregado al carrito");
              _kartCubit.addKart(_product);
            },
            child: const Text(
              "Agregar al carrito",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
