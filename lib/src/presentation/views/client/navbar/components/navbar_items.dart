import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/bloc/navbar/navbar_cubit.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';

class NavbarItems extends StatelessWidget {
  const NavbarItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavbarCubit, NavbarState>(
      builder: (context, state) {
        return ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
          child: CustomNavigationBar(
            backgroundColor: AppColors.kBackGround,
            currentIndex: state.index,
            elevation: 0.0,
            bubbleCurve: Curves.easeIn,
            strokeColor: Colors.transparent,
            onTap: context.read<NavbarCubit>().selectIndex,
            items: [
              CustomNavigationBarItem(
                icon: Image(
                  image: Image.asset("assets/images/iconos/home.png").image,
                ),
              ),
              CustomNavigationBarItem(
                icon: Image(
                  image: Image.asset("assets/images/iconos/heart.png").image,
                ),
              ),
              CustomNavigationBarItem(
                icon: Image(
                  image: Image.asset("assets/images/iconos/person.png").image,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
