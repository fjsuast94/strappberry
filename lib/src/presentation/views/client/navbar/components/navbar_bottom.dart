import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/bloc/navbar/navbar_cubit.dart';
import 'package:strappberry/src/presentation/views/client/navbar/components/navbar_items.dart';

class NavbarBottom extends StatelessWidget {
  const NavbarBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavbarCubit, NavbarState>(
      builder: (context, state) {
        return const AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: NavbarItems(),
        );
      },
    );
  }
}
