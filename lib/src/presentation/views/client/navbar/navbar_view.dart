import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/bloc/navbar/navbar_cubit.dart';
import 'package:strappberry/src/presentation/views/client/favorite/favorite_view.dart';
import 'package:strappberry/src/presentation/views/client/home/home_view.dart';
import 'package:strappberry/src/presentation/views/client/navbar/components/navbar_bottom.dart';
import 'package:strappberry/src/presentation/views/client/setting/setting_view.dart';

class NavbarView extends StatelessWidget {
  static const String routeName = '/navbar';
  const NavbarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavbarCubit, NavbarState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async => context.read<NavbarCubit>().willPop(),
          child: Scaffold(
            extendBody: true,
            body: PageView(
              physics: const NeverScrollableScrollPhysics(),
              children: const [
                HomeView(),
                FavoriteView(),
                SettingView(),
              ],
              controller: context.read<NavbarCubit>().pageController,
            ),
            bottomNavigationBar: const NavbarBottom(),
          ),
        );
      },
    );
  }
}
