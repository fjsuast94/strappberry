import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/favorite.dart';
import 'package:strappberry/src/presentation/bloc/favorite/favorite_cubit.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/widgets/cards/card_refresh.dart';
import 'package:strappberry/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';

class FavoriteView extends StatelessWidget {
  static const String routeName = "/client-favorite";
  const FavoriteView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<FavoriteCubit>();

    final paginationController = cubit.paginateController;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.kBackGround,
        iconTheme: const IconThemeData(
          color: AppColors.kIconColor,
        ),
        centerTitle: true,
        title: const Text(
          "Mi favoritos",
          style: TextStyle(
            fontSize: 22,
            color: AppColors.kBackGroundSecondary,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: RefreshSliverScrollView(
          onRefresh: () => paginationController.refresh(),
          slivers: [
            const SliverToBoxAdapter(child: SizedBox(height: 0)),
            PagedSliverList.separated(
              pagingController: paginationController,
              builderDelegate: PagedChildBuilderDelegate<FavoriteModel>(
                itemBuilder: (context, favorite, index) => Container(
                  height: 100,
                  decoration: const BoxDecoration(
                    color: AppColors.kBackGround,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(File(favorite.product.image)),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                child: Text(
                                  favorite.product.name.toUpperCase(),
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              SizedBox(
                                child: Text(
                                  favorite.product.description,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Helper.moneyFormat(favorite.product.price),
                                    style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                  title: Text("${paginationController.error}"),
                ),
                noItemsFoundIndicatorBuilder: (context) => const Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: CardRefresh(
                    title: Text("Aun no tienes favoritos en tu lista"),
                  ),
                ),
              ),
              separatorBuilder: (context, index) => const SizedBox(
                height: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
