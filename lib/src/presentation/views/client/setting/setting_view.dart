import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/views/client/setting/cubit/setting_cubit.dart';
import 'package:strappberry/src/presentation/widgets/alerts/alert_actions.dart';

class SettingView extends StatelessWidget {
  static const String routeName = "/client-setting";
  const SettingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => SettingCubit(
        auth: context.read(),
      )..init(),
      child: Scaffold(
        body: SafeArea(
          child: BlocBuilder<SettingCubit, SettingState>(
            builder: (context, state) {
              final user = state.user;
              if (user == null) {
                return const Center(
                  child: CircularProgressIndicator(
                    backgroundColor: AppColors.kBackGroundSecondary,
                  ),
                );
              }
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipOval(
                          child: Hero(
                            tag: "user.photo",
                            child: Container(
                              height: responsive.heightPercent(10),
                              width: responsive.widthPercent(20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                image: const DecorationImage(
                                  image: CachedNetworkImageProvider(
                                    "https://autocinemaimperial.com/images/avatar-masculino.png",
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 20),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 20),
                              Text(
                                user.name,
                                style: TextStyle(
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(0.7),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Text(
                                user.rol,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.zero,
                      leading: const Icon(Icons.email_outlined),
                      title: Text(
                        user.email,
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w400,
                          color: Colors.black.withOpacity(0.5),
                        ),
                      ),
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        "Cerrar Sessión",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w400,
                          color: Colors.black.withOpacity(0.5),
                        ),
                      ),
                      trailing: const Icon(Icons.logout),
                      onTap: () {
                        Routes.dialog(
                          AlertAction(
                            title: "¿Deseas cerrar sesión?",
                            onPressYes: () async {
                              await context.read<SettingCubit>().logout();
                              Routes.offAllNamed('/login');
                            },
                          ),
                        );
                      },
                      tileColor: Colors.transparent,
                      selectedTileColor: Colors.transparent,
                      dense: true,
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
