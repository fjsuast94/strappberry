import 'package:bloc/bloc.dart';
import 'package:strappberry/src/data/model/user.dart';
import 'package:strappberry/src/data/usecase/auth.dart';

part 'setting_state.dart';

class SettingCubit extends Cubit<SettingState> {
  SettingCubit({
    required AuthCase auth,
  })  : _auth = auth,
        super(SettingState());

  final AuthCase _auth;

  Future<void> init() async {
    final user = await _auth.getUser();
    emit(state.copyWith(user: user));
  }

  Future<void> logout() async {
    await _auth.logOut();
  }
}
