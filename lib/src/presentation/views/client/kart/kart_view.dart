import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/kart.dart';
import 'package:strappberry/src/presentation/bloc/kart/kart_cubit.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/widgets/alerts/alert_actions.dart';
import 'package:strappberry/src/presentation/widgets/cards/card_refresh.dart';
import 'package:strappberry/src/presentation/widgets/form-bloc/numeric_step_button.dart';
import 'package:strappberry/src/presentation/widgets/qr/qr_shared/qr_shared.dart';
import 'package:strappberry/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';

class KartView extends StatelessWidget {
  static const String routeName = "/client-kart";
  const KartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<KartCubit>();
    final paginationController = cubit.paginateController;

    double _total = 0.0;
    for (var item in cubit.state.karts) {
      _total = _total + item.cantidad * item.price;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.kBackGround,
        iconTheme: const IconThemeData(
          color: AppColors.kIconColor,
        ),
        centerTitle: true,
        title: const Text(
          "Mi carrito",
          style: TextStyle(
            fontSize: 22,
            color: AppColors.kBackGroundSecondary,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: RefreshSliverScrollView(
          onRefresh: () => paginationController.refresh(),
          slivers: [
            const SliverToBoxAdapter(child: SizedBox(height: 0)),
            PagedSliverList.separated(
              pagingController: paginationController,
              builderDelegate: PagedChildBuilderDelegate<KartModel>(
                itemBuilder: (context, kart, index) => Container(
                  height: 100,
                  decoration: const BoxDecoration(
                    color: AppColors.kBackGround,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(File(kart.product.image)),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                child: Text(
                                  kart.product.name,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              SizedBox(
                                child: Text(
                                  kart.product.description,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Helper.moneyFormat(kart.product.price),
                                    style: const TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  NumericStepButton(
                                    maxValue: 100,
                                    minValue: 1,
                                    total: kart.cantidad,
                                    onChanged: (counter) {
                                      cubit.updateKart(index, kart, counter);
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                  title: Text("${paginationController.error}"),
                ),
                noItemsFoundIndicatorBuilder: (context) => const Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: CardRefresh(
                    title: Text("No hay productos disponibles"),
                  ),
                ),
              ),
              separatorBuilder: (context, index) => const SizedBox(
                height: 10,
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: SizedBox(
          height: 100,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Total",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    Helper.moneyFormat(_total),
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: TextButton(
                  onPressed: () async {
                    await Routes.dialog(
                      AlertAction(
                        title:
                            "¿Estas seguro de realizar la compra.? Se te generará un codigo Qr para verificar tus productos",
                        onPressYes: () async {
                          Routes.dialog(
                            ShareQrCard(
                              qrValue: QrCardModel(
                                boletoQr: "cubit.state.kart!.product.uuid",
                                terminos: "",
                                recomendacion: "",
                              ),
                            ),
                          );
                          //cubit.shopping();
                        },
                      ),
                    );
                  },
                  child: const Text(
                    "Comprar Ahora",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
