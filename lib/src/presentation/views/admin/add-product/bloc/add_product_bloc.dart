import 'dart:io';

import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:rxdart/rxdart.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/usecase/product.dart';
import 'package:strappberry/src/presentation/core/utils/validator_string.dart';
import 'package:uuid/uuid.dart';

class AddProductBloc extends FormBloc<String, String> {
  AddProductBloc({
    required ImagePicker picker,
    required ProductUseCase product,
    this.productModel,
  })  : _picker = picker,
        _product = product {
    adderror("");
    addFieldBlocs(fieldBlocs: [name, price, description]);

    if (productModel != null) {
      name.updateValue(productModel!.name);
      price.updateValue(productModel!.price.toString());
      description.updateValue(productModel!.description);
      addImageGallery(File(productModel!.image));
      imagePath = productModel!.image;
    }
  }

  final ImagePicker _picker;
  final ProductUseCase _product;
  final ProductModel? productModel;

  String imagePath = "";

  final BehaviorSubject<File?> _imageGallery = BehaviorSubject<File?>();
  Function(File?) get addImageGallery => _imageGallery.sink.add;
  Stream<File?> get getImageGallery => _imageGallery.stream;
  File? get imageGallery => _imageGallery.value;

  final BehaviorSubject<String> _error = BehaviorSubject<String>();
  Function(String) get adderror => _error.sink.add;
  Stream<String> get getError => _error.stream;
  String get error => _error.value;

  final name = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateName,
    ],
  );

  final price = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
    ],
  );

  final description = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  @override
  void onSubmitting() async {
    if (imagePath.isEmpty) {
      adderror("La imagen es requerida");
      emitFailure();
      return;
    }
    adderror("");
    await Future.delayed(const Duration(milliseconds: 900));
    if (productModel == null) {
      var uuid = const Uuid();
      _product.createProducts(ProductModel.fromJson({
        "uuid": uuid.v4(),
        "name": name.value,
        "price": price.valueToDouble,
        "description": description.value,
        "image": imagePath,
      }));
      name.clear();
      price.clear();
      description.clear();
      addImageGallery(null);
      emitSuccess(canSubmitAgain: true);
      return;
    }
    _product.updateProduct(ProductModel.fromJson({
      "uuid": productModel!.uuid,
      "name": name.value,
      "price": price.valueToDouble,
      "description": description.value,
      "image": imagePath,
    }));

    emitSuccess(canSubmitAgain: true);
  }

  Future<bool> openGallery() async {
    final picture = await _picker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 500,
      maxWidth: 640,
    );

    if (picture == null) return false;

    File _image = File(picture.path);
    addImageGallery(_image);

    Directory appDocDir = await getApplicationDocumentsDirectory();
    String path = appDocDir.path;
    String fileName = basename(picture.path);
    File localImage = await File(picture.path).copy('$path/$fileName');
    imagePath = localImage.path;
    return true;
  }

  Future<bool> openCamara() async {
    final picture = await _picker.pickImage(
      source: ImageSource.camera,
      maxHeight: 500,
      maxWidth: 640,
    );

    if (picture == null) return false;
    File _image = File(picture.path);
    addImageGallery(_image);
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String path = appDocDir.path;
    String fileName = basename(picture.path);
    File localImage = await File(picture.path).copy('$path/$fileName');

    imagePath = localImage.path;

    return true;
  }

  void deleteImage() {
    addImageGallery(null);
  }
}
