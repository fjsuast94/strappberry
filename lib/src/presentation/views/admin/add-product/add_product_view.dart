import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/views/admin/add-product/bloc/add_product_bloc.dart';
import 'package:strappberry/src/presentation/views/admin/products/cubit/product_cubit.dart';
import 'package:strappberry/src/presentation/widgets/buttons/bottom_navigation_button.dart';
import 'package:strappberry/src/presentation/widgets/form-bloc/input_text_cupertino.dart';
import 'package:strappberry/src/presentation/widgets/form-bloc/un_focus_form.dart';
import 'package:strappberry/src/presentation/widgets/loading/loading_app.dart';

class AddProductView extends StatelessWidget {
  static const String routeName = "/add-product";
  const AddProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<ProductCubit>();
    return BlocProvider(
      create: (context) => AddProductBloc(
        picker: ImagePicker(),
        product: context.read(),
        productModel: cubit.state.product,
      ),
      child: Builder(
        builder: (context) {
          final bloc = context.read<AddProductBloc>();
          return FormBlocListener<AddProductBloc, String, String>(
            onSubmitting: (context, state) => LoadingApp.show(context),
            onSuccess: (context, state) {
              cubit.paginateController.refresh();
              LoadingApp.hide(context);
            },
            onFailure: (context, state) {
              LoadingApp.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: UnFocusForm(
              child: Scaffold(
                resizeToAvoidBottomInset: true,
                appBar: AppBar(
                  centerTitle: true,
                  title: const Text("Agregar producto"),
                ),
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      children: [
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: bloc.openGallery,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StreamBuilder(
                                stream: bloc.getImageGallery,
                                initialData: null,
                                builder: (BuildContext context, AsyncSnapshot snapshot) {
                                  final imageGallery = snapshot.data as File?;
                                  return AnimatedSwitcher(
                                    duration: const Duration(milliseconds: 700),
                                    child: imageGallery == null
                                        ? Center(
                                            child: DottedBorder(
                                            color: Colors.black.withOpacity(0.3),
                                            strokeWidth: 1,
                                            child: Container(
                                              width: 343,
                                              height: 126,
                                              decoration: const BoxDecoration(
                                                color: AppColors.kBackGround,
                                              ),
                                              child: Center(
                                                child: Image(
                                                  width: 60,
                                                  height: 70,
                                                  image: Image.asset(
                                                    "assets/images/iconos/file.png",
                                                  ).image,
                                                ),
                                              ),
                                            ),
                                          ))
                                        : Container(
                                            width: 343,
                                            height: 126,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: Image.file(imageGallery).image,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                  );
                                },
                              ),
                              AnimatedSwitcher(
                                  duration: const Duration(milliseconds: 700),
                                  child: StreamBuilder<String>(
                                    builder: (context, data) {
                                      return data.data!.isEmpty
                                          ? const SizedBox.shrink()
                                          : Text(
                                              data.data!,
                                              style: const TextStyle(color: AppColors.kCancel),
                                            );
                                    },
                                    initialData: "",
                                    stream: bloc.getError,
                                  ))
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        InputTextCupertino(
                          textFieldBloc: bloc.name,
                          placeholder: 'Nombre',
                          keyboardType: TextInputType.text,
                        ),
                        InputTextCupertino(
                          textFieldBloc: bloc.price,
                          placeholder: 'Precio',
                          keyboardType: const TextInputType.numberWithOptions(
                            decimal: true,
                          ),
                        ),
                        Container(
                          color: AppColors.kBackGround,
                          padding: const EdgeInsets.only(bottom: 40.0),
                          child: InputTextCupertino(
                            textFieldBloc: bloc.description,
                            placeholder: 'Descripción',
                            textInputAction: TextInputAction.newline,
                            keyboardType: TextInputType.multiline,
                            minLines: 1,
                            maxLines: 99,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                bottomNavigationBar: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: BottomNavigationButtom(
                      titleNext: const Text(
                        "Guardar producto",
                        style: TextStyle(fontSize: 20),
                      ),
                      onNext: () {
                        //print(bloc.submit());
                        bloc.submit();
                      },
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
