part of 'product_cubit.dart';

class ProductState {
  ProductState({
    this.loading = false,
    this.product,
    this.products = const <ProductModel>[],
  });

  final ProductModel? product;
  final List<ProductModel> products;
  final bool loading;

  ProductState copyWith({
    bool? loading,
    ProductModel? product,
    List<ProductModel>? products,
  }) =>
      ProductState(
        loading: loading ?? this.loading,
        product: product,
        products: products ?? this.products,
      );
}
