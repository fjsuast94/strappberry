import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/data/usecase/product.dart';

part 'product_state.dart';

class ProductCubit extends Cubit<ProductState> {
  ProductCubit({
    required PagingController<int, ProductModel> paginateController,
    required ProductUseCase productUse,
    required AuthCase auth,
  })  : _paginateController = paginateController,
        _productUse = productUse,
        _auth = auth,
        super(ProductState());

  final PagingController<int, ProductModel> _paginateController;
  PagingController<int, ProductModel> get paginateController => _paginateController;

  final ProductUseCase _productUse;
  final AuthCase _auth;

  static const _pageSize = 10;

  Future<void> init() async {
    _paginateController.addPageRequestListener(_paginationListener);
  }

  void _paginationListener(int pageKey) async {
    final response = await _productUse.getProducts();
    if (response.message!.isNotEmpty) {
      _paginateController.error = response.message;
      return;
    }
    final isLastPage = response.data.length <= _pageSize;
    isLastPage
        ? _paginateController.appendLastPage(response.data)
        : _paginateController.appendPage(response.data, pageKey + 1);
  }

  void setProduct(ProductModel? product) async {
    emit(state.copyWith(product: product));
  }

  void deleteProduct(int index) async {
    _productUse.deleteProducts(index);
    _paginateController.refresh();
  }

  void logout() async {
    _auth.logOut();
  }
}
