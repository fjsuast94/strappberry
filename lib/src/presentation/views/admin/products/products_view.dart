import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/views/admin/add-product/add_product_view.dart';
import 'package:strappberry/src/presentation/views/admin/products/cubit/product_cubit.dart';
import 'package:strappberry/src/presentation/widgets/alerts/alert_actions.dart';
import 'package:strappberry/src/presentation/widgets/cards/card_refresh.dart';
import 'package:strappberry/src/presentation/widgets/icons/avatar.dart';
import 'package:strappberry/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';

class ProductsView extends StatelessWidget {
  static const String routeName = "/products";
  const ProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => ProductCubit(
        paginateController: PagingController(firstPageKey: 1),
        productUse: context.read(),
        auth: context.read(),
      )..init(),
      child: BlocBuilder<ProductCubit, ProductState>(
        builder: (context, state) {
          final cubit = context.read<ProductCubit>();
          final paginationController = cubit.paginateController;
          return WillPopScope(
            onWillPop: () async {
              Routes.dialog(
                AlertAction(
                  title: "¿Deseas cerrar sesión?",
                  onPressYes: () async {
                    cubit.logout();
                    Routes.offAllNamed('/login');
                  },
                ),
              );
              return false;
            },
            child: Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: const Text(
                  "Productos",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                actions: [
                  ClipOval(
                    child: Hero(
                      tag: "user-tag",
                      child: AvatarIcon(
                        profileImage: "https://autocinemaimperial.com/images/avatar-masculino.png",
                        marginColor: Colors.transparent,
                        height: responsive.widthPercent(8.5),
                        width: responsive.widthPercent(13),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: () => Routes.goToName("/admin-setting"),
                            child: const SizedBox(),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                ],
              ),
              body: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: RefreshSliverScrollView(
                  onRefresh: () => paginationController.refresh(),
                  slivers: [
                    const SliverToBoxAdapter(child: SizedBox(height: 0)),
                    PagedSliverList.separated(
                      pagingController: paginationController,
                      builderDelegate: PagedChildBuilderDelegate<ProductModel>(
                        itemBuilder: (context, product, index) => Container(
                          height: 100,
                          decoration: const BoxDecoration(
                            color: AppColors.kBackGround,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: FileImage(File(product.image)),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        child: Text(
                                          product.name.toUpperCase(),
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        child: Text(
                                          product.description,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            Helper.moneyFormat(product.price),
                                            style: const TextStyle(
                                              fontSize: 15,
                                              color: Colors.black,
                                            ),
                                          ),
                                          Expanded(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    cubit.setProduct(product);
                                                    Routes.to(
                                                      () => BlocProvider.value(
                                                        value: cubit,
                                                        child: const AddProductView(),
                                                      ),
                                                    );
                                                  },
                                                  child: const Icon(FontAwesomeIcons.edit),
                                                ),
                                                const SizedBox(width: 20),
                                                GestureDetector(
                                                  onTap: () {
                                                    Routes.dialog(
                                                      AlertAction(
                                                        title: "¿Deseas eliminar el producto?",
                                                        onPressYes: () async {
                                                          cubit.deleteProduct(index);
                                                          Routes.back();
                                                        },
                                                      ),
                                                    );
                                                  },
                                                  child: const Icon(FontAwesomeIcons.trash),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                          title: Text("${paginationController.error}"),
                        ),
                        noItemsFoundIndicatorBuilder: (context) => const Padding(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          child: CardRefresh(
                            title: Text("No hay productos disponibles"),
                          ),
                        ),
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 10,
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButton: FloatingActionButton(
                child: const Icon(
                  Icons.add,
                  size: 25,
                  color: Colors.white,
                ),
                onPressed: () {
                  cubit.setProduct(null);
                  Routes.to(
                    () => BlocProvider.value(
                      value: cubit,
                      child: const AddProductView(),
                    ),
                  );
                },
                splashColor: Colors.transparent,
                elevation: 0,
                backgroundColor: AppColors.kButtonColor,
              ),
            ),
          );
        },
      ),
    );
  }
}
