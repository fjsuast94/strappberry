part of 'setting_cubit.dart';

class SettingState {
  SettingState({
    this.biometric = false,
    this.isAndroid8OrInferiror = false,
    this.user,
  });
  final bool biometric;
  final bool isAndroid8OrInferiror;
  final UserModel? user;

  SettingState copyWith({
    bool? biometric,
    UserModel? user,
    bool? isAndroid8OrInferiror,
  }) =>
      SettingState(
        biometric: biometric ?? this.biometric,
        isAndroid8OrInferiror: isAndroid8OrInferiror ?? this.isAndroid8OrInferiror,
        user: user ?? this.user,
      );
}
