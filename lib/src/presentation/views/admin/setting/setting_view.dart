import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/views/admin/setting/cubit/setting_cubit.dart';
import 'package:strappberry/src/presentation/widgets/alerts/alert_actions.dart';

class SettingAdminView extends StatelessWidget {
  static const String routeName = "/admin-setting";
  const SettingAdminView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => SettingCubit(
        auth: context.read(),
      )..init(),
      child: Scaffold(
        appBar: AppBar(),
        body: SafeArea(
          child: BlocBuilder<SettingCubit, SettingState>(
            builder: (context, state) {
              final user = state.user;
              if (user == null) {
                return const Center(
                  child: CircularProgressIndicator(
                    backgroundColor: AppColors.kBackGroundSecondary,
                  ),
                );
              }
              return Container(
                height: 250,
                decoration: const BoxDecoration(
                  color: AppColors.kBackGroundSecondary,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipOval(
                            child: Hero(
                              tag: "user.photo",
                              child: Container(
                                height: responsive.heightPercent(10),
                                width: responsive.widthPercent(20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: const DecorationImage(
                                    image: CachedNetworkImageProvider(
                                      "https://autocinemaimperial.com/images/avatar-masculino.png",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(height: 20),
                                Text(
                                  user.name,
                                  style: const TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  user.rol,
                                  style: const TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: const Icon(
                          Icons.email_outlined,
                          color: Colors.white,
                        ),
                        title: Text(
                          user.email,
                          style: const TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text(
                          "Cerrar Sessión",
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                        trailing: const Icon(
                          Icons.logout,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Routes.dialog(
                            AlertAction(
                              title: "¿Deseas cerrar sesión?",
                              onPressYes: () async {
                                await context.read<SettingCubit>().logout();
                                Routes.offAllNamed('/login');
                              },
                            ),
                          );
                        },
                        tileColor: Colors.transparent,
                        selectedTileColor: Colors.transparent,
                        dense: true,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
