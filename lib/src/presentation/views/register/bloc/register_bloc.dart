import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/presentation/core/utils/validator_string.dart';

class RegisterBloc extends FormBloc<String, String> {
  RegisterBloc({
    required AuthCase auth,
  }) : _auth = auth {
    addFieldBlocs(fieldBlocs: [name, email, password, confirmPassword]);
    confirmPassword
      ..addValidators([ValidatorString.confirmPassword(password)])
      ..subscribeToFieldBlocs([password]);
  }

  final AuthCase _auth;

  final name = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateName,
    ],
  );

  final email = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.emailFormat,
    ],
  );

  final password = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  final confirmPassword = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  @override
  void onSubmitting() async {
    await Future.delayed(const Duration(milliseconds: 1000));
    final response = await _auth.register(name.value!, email.value!, password.value!);
    if (response.status!) {
      emitSuccess(canSubmitAgain: true);
    } else {
      emitFailure(failureResponse: response.message);
    }
  }
}
