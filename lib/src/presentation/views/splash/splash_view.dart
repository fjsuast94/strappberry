import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/app_colors.dart';
import 'package:strappberry/src/presentation/core/theme/responsive.dart';
import 'package:strappberry/src/presentation/views/splash/cubit/splash_cubit.dart';

class SplashView extends StatelessWidget {
  static const String routeName = "/spash";
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Scaffold(
      backgroundColor: AppColors.kBackGroundSecondary,
      body: BlocProvider(
        create: (context) => SplashCubit(
          auth: context.read(),
        )..init(),
        child: BlocConsumer<SplashCubit, SplashState>(
          listener: (context, state) {
            if (!state.loading) {
              Routes.offAndToNamed(state.page);
            }
          },
          builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: responsive.height / 2,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage("assets/images/logos/logo_strappberry.png"),
                      ),
                    ),
                  ),
                  const SizedBox(height: 40),
                  CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.white.withOpacity(0.7),
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
