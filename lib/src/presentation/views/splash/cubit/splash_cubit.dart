import 'package:bloc/bloc.dart';
import 'package:strappberry/src/data/usecase/auth.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit({
    required AuthCase auth,
  })  : _auth = auth,
        super(SplashState());

  final AuthCase _auth;

  Future<void> init() async {
    await _auth.loadUsers();
    final user = await _auth.getUser();
    // Evaluar si ya existe datos dentro de la app, si existe datos enviar al login
    // Caso contrario si para enviar al registro
    emit(state.copyWith(loading: true));
    await Future.delayed(const Duration(milliseconds: 1000));
    final String page = user.name.isEmpty
        ? "/login"
        : user.rol == "Admin"
            ? "/products"
            : "/navbar";

    emit(state.copyWith(page: page, loading: false));
  }
}
