part of 'splash_cubit.dart';

class SplashState {
  SplashState({
    this.loading = false,
    this.page = "/register-home",
    this.splashRoute = SplashRoute.boarding,
  });
  final String page;
  final bool loading;
  final SplashRoute splashRoute;

  SplashState copyWith({
    bool? loading,
    SplashRoute? splashRoute,
    String? page,
  }) =>
      SplashState(
        loading: loading ?? this.loading,
        splashRoute: splashRoute ?? this.splashRoute,
        page: page ?? this.page,
      );
}

enum SplashRoute { boarding, login, loading }
