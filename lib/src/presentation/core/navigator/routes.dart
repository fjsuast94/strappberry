import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:strappberry/src/presentation/core/utils/helper.dart';
import 'package:strappberry/src/presentation/views/admin/add-product/add_product_view.dart';
import 'package:strappberry/src/presentation/views/admin/products/products_view.dart';
import 'package:strappberry/src/presentation/views/admin/setting/setting_view.dart';
import 'package:strappberry/src/presentation/views/client/favorite/favorite_view.dart';
import 'package:strappberry/src/presentation/views/client/home/home_view.dart';
import 'package:strappberry/src/presentation/views/client/kart/kart_view.dart';
import 'package:strappberry/src/presentation/views/client/navbar/navbar_view.dart';
import 'package:strappberry/src/presentation/views/client/product/product_view.dart';
import 'package:strappberry/src/presentation/views/login/login_view.dart';
import 'package:strappberry/src/presentation/views/register/register_view.dart';
import 'package:strappberry/src/presentation/views/splash/splash_view.dart';

class Routes {
  static List<GetPage> routes = [
    GetPage(
      name: SplashView.routeName,
      page: () => const SplashView(),
    ),
    GetPage(
      name: LoginView.routeName,
      page: () => const LoginView(),
    ),
    GetPage(
      name: RegisterView.routeName,
      page: () => const RegisterView(),
    ),
    GetPage(
      name: ProductsView.routeName,
      page: () => const ProductsView(),
    ),
    GetPage(
      name: AddProductView.routeName,
      page: () => const AddProductView(),
    ),
    GetPage(
      name: HomeView.routeName,
      page: () => const HomeView(),
    ),
    GetPage(
      name: KartView.routeName,
      page: () => const KartView(),
    ),
    GetPage(
      name: ProductView.routeName,
      page: () => const ProductView(),
    ),
    GetPage(
      name: FavoriteView.routeName,
      page: () => const FavoriteView(),
    ),
    GetPage(
      name: NavbarView.routeName,
      page: () => const NavbarView(),
    ),
    GetPage(
      name: SettingAdminView.routeName,
      page: () => const SettingAdminView(),
    ),
  ];

  static goToName(String name, {dynamic arguments}) {
    routes.any((e) => e.name == name)
        ? Get.toNamed(
            name,
            arguments: arguments,
            preventDuplicates: false,
          )
        : Helper.error(
            message: "El módulo no esta disponible o no tienes acceso a ello.",
          );
  }

  static to(dynamic page, {bool fullscreenDialog = false}) {
    Get.to(page, fullscreenDialog: fullscreenDialog);
  }

  static offAndToNamed(String name, {dynamic arguments}) {
    routes.any((e) => e.name == name)
        ? Get.offAndToNamed(
            name,
            arguments: arguments,
          )
        : Helper.error(
            message: "El módulo no esta disponible o no tienes acceso a ello.",
          );
  }

  static offAllNamed(String name, {dynamic arguments}) {
    routes.any((e) => e.name == name)
        ? Get.offAllNamed(
            name,
            arguments: arguments,
          )
        : Helper.error(
            message: "El módulo no esta disponible o no tienes acceso a ello.",
          );
  }

  static Future<dynamic> fullDialog(dynamic page, {dynamic arguments}) async {
    return await Get.to(
      page,
      arguments: arguments,
      fullscreenDialog: true,
    );
  }

  static void back({dynamic result}) {
    return Get.back(
      result: result,
    );
  }

  static Future<dynamic> dialog(
    Widget widget, {
    bool barrierDismissible = true,
    Color? barrierColor,
    bool useSafeArea = true,
    Object? arguments,
    Duration? transitionDuration,
    Curve? transitionCurve,
    String? name,
    RouteSettings? routeSettings,
  }) async {
    return await Get.dialog(
      widget,
      barrierDismissible: barrierDismissible,
      barrierColor: barrierColor,
      useSafeArea: useSafeArea,
      arguments: arguments,
      transitionDuration: transitionDuration,
      transitionCurve: transitionCurve,
      name: name,
      routeSettings: routeSettings,
    );
  }
}
