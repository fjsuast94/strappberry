import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
//import 'package:get/get_utils/get_utils.dart';

class Helper {
  static const String _title = 'Strappberry';
  static const int _duration = 3000;

  static Future<void> success({
    String title = _title,
    required String? message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message!,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.green.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  static Future<void> error({
    String title = _title,
    required String message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.red.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  static Future<void> warning({
    String title = _title,
    required String message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.yellow.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  /* static Future<void> launchInBrowser(String url) async {
    url.isURL
        ? await canLaunch(url)
            ? await launch(url, forceSafariVC: false)
            : Helper.error(message: 'No se pudo acceder a esta url $url')
        : Helper.error(message: 'Favor de ingresar una url valida');
  } */

  static String moneyFormat(dynamic money) {
    final formatCurrency = NumberFormat.simpleCurrency();
    return formatCurrency.format(money);
  }

  static String dateTimeFormat() {
    DateTime now = DateTime.now();
    return DateFormat('yyyy-MM-dd – kk:mm').format(now);
  }

  static String numberFormat(dynamic number) {
    final formatCurrency = NumberFormat();
    return formatCurrency.format(number);
  }

  static String dateFormat(String? date) {
    if (date == null || date.isEmpty) {
      return "Fecha no disponible";
    }

    final inputFormat = DateFormat('yyyy-MM-dd');
    final inputDate = inputFormat.parse(date);
    final outputFormat = DateFormat('dd/MM/yyyy');
    final outputDate = outputFormat.format(inputDate);
    return outputDate;
  }

  static String dateFormatHours(String? date) {
    if (date == null || date.isEmpty) {
      return "Fecha no disponible";
    }

    final inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    final inputDate = inputFormat.parse(date);
    final outputFormat = DateFormat('HH:mm');
    final outputDate = outputFormat.format(inputDate);
    return outputDate;
  }

  static int randInt({int minLimit = 1, int maxLimit = 100}) {
    Random random = Random();
    return random.nextInt(maxLimit) + minLimit;
  }
}
