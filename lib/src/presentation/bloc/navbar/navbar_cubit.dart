import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:strappberry/src/data/model/user.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';

part 'navbar_state.dart';

class NavbarCubit extends Cubit<NavbarState> {
  NavbarCubit({
    required ScrollController scroll,
    required StorageRepository storage,
    required PageController pageController,
  })  : _scroll = scroll,
        _storage = storage,
        _pageController = pageController,
        super(NavbarState());

  final ScrollController _scroll;
  final StorageRepository _storage;
  final PageController _pageController;

  ScrollController get scroll => _scroll;
  PageController get pageController => _pageController;

  void init() async {
    emit(state.copyWith(user: _storage.user));
    _scroll.addListener(_listener);
  }

  @override
  Future<void> close() {
    _scroll.removeListener(_listener);
    return super.close();
  }

  void changeUser() {
    emit(state.copyWith(user: _storage.user));
  }

  bool willPop() {
    if (_pageController.page != 0) {
      _pageController.jumpToPage(0);
      emit(state.copyWith(index: 0));
      return false;
    }
    return true;
  }

  void selectIndex(int index) {
    _pageController.jumpToPage(index);
    emit(state.copyWith(index: index));
  }

  void _listener() {
    if (_scroll.position.userScrollDirection == ScrollDirection.reverse && state.show) {
      emit(state.copyWith(show: false));
    } else if (_scroll.position.userScrollDirection == ScrollDirection.forward && !state.show) {
      emit(state.copyWith(show: true));
    }
  }
}
