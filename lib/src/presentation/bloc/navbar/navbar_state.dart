part of 'navbar_cubit.dart';

class NavbarState {
  NavbarState({
    this.index = 0,
    this.show = true,
    this.user,
  });

  final int index;
  final bool show;
  final UserModel? user;

  NavbarState copyWith({
    int? index,
    bool? show,
    UserModel? user,
  }) =>
      NavbarState(
        index: index ?? this.index,
        show: show ?? this.show,
        user: user ?? this.user,
      );
}
