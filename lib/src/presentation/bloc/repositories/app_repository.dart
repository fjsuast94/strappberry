import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/repository/local/http/http_dio.dart';
import 'package:strappberry/src/data/repository/local/storage/storage_repository.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/data/usecase/favorite.dart';
import 'package:strappberry/src/data/usecase/kart.dart';
import 'package:strappberry/src/data/usecase/product.dart';
import 'package:strappberry/src/presentation/bloc/favorite/favorite_cubit.dart';
import 'package:strappberry/src/presentation/bloc/kart/kart_cubit.dart';
import 'package:strappberry/src/presentation/bloc/navbar/navbar_cubit.dart';

class AppRepository extends StatelessWidget {
  const AppRepository({
    Key? key,
    required this.child,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final dio = Dio();
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<StorageRepository>(
          create: (context) => StorageRepository(
            storage: GetStorage(),
          ),
        ),
        RepositoryProvider<HttpDio>(
          create: (context) => HttpDio(
            baseUrl: context.read<StorageRepository>().api,
            dio: dio,
          ),
        ),
        RepositoryProvider<AuthCase>(
          create: (context) => AuthCase(
            httpDio: context.read(),
            storage: context.read(),
          ),
        ),
        RepositoryProvider<ProductUseCase>(
          create: (context) => ProductUseCase(
            storage: context.read(),
          ),
        ),
        RepositoryProvider<FavoritetUseCase>(
          create: (context) => FavoritetUseCase(
            storage: context.read(),
          ),
        ),
        RepositoryProvider<KartUseCase>(
          create: (context) => KartUseCase(
            storage: context.read(),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => NavbarCubit(
              pageController: PageController(initialPage: 0),
              storage: context.read(),
              scroll: ScrollController(),
            )..init(),
          ),
          BlocProvider(
            create: (context) => KartCubit(
              kart: context.read(),
              auth: context.read(),
              paginateController: PagingController(firstPageKey: 1),
            )..init(),
          ),
          BlocProvider(
            create: (context) => FavoriteCubit(
              favoriteUse: context.read(),
              paginateController: PagingController(firstPageKey: 1),
            )..init(),
          ),
        ],
        child: child,
      ),
    );
  }
}
