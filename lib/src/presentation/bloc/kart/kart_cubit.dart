import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/kart.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/usecase/auth.dart';
import 'package:strappberry/src/data/usecase/kart.dart';

part 'kart_state.dart';

class KartCubit extends Cubit<KartState> {
  KartCubit({
    required KartUseCase kart,
    required AuthCase auth,
    required PagingController<int, KartModel> paginateController,
  })  : _kart = kart,
        _auth = auth,
        _paginateController = paginateController,
        super(KartState());

  final KartUseCase _kart;
  final AuthCase _auth;

  final PagingController<int, KartModel> _paginateController;
  PagingController<int, KartModel> get paginateController => _paginateController;

  static const _pageSize = 10;

  Future<void> init() async {
    _paginateController.addPageRequestListener(_paginationListener);
  }

  void _paginationListener(int pageKey) async {
    final response = await _kart.getKarts();
    emit(state.copyWith(karts: response.data));
    if (response.message!.isNotEmpty) {
      _paginateController.error = response.message;
      return;
    }
    final isLastPage = response.data.length <= _pageSize;
    isLastPage
        ? _paginateController.appendLastPage(response.data)
        : _paginateController.appendPage(response.data, pageKey + 1);
  }

  Future<void> addKart(ProductModel _product) async {
    final user = await _auth.getUser();
    _kart.createkarts(
      KartModel.fromJson({
        "cantidad": 1,
        "price": _product.price,
        "product": _product.toJson(),
        "user": user.toJson(),
      }),
    );
    _paginateController.refresh();
  }

  Future<void> updateKart(int index, KartModel _kartModel, int counter) async {
    _kart.updatekarts(
      index,
      KartModel.fromJson({
        "cantidad": counter,
        "price": _kartModel.price,
        "product": _kartModel.product.toJson(),
        "user": _kartModel.user.toJson(),
      }),
    );
    _paginateController.refresh();
  }

  Future<void> shopping() async {
    _kart.clearKarts();
    _paginateController.refresh();
  }
}
