part of 'kart_cubit.dart';

class KartState {
  KartState({
    this.kart,
    this.karts = const <KartModel>[],
  });

  KartModel? kart;
  List<KartModel> karts;

  KartState copyWith({
    KartModel? kart,
    List<KartModel>? karts,
  }) =>
      KartState(
        kart: kart ?? this.kart,
        karts: karts ?? this.karts,
      );
}
