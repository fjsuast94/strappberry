import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:strappberry/src/data/model/favorite.dart';
import 'package:strappberry/src/data/model/product.dart';
import 'package:strappberry/src/data/usecase/favorite.dart';

part 'favorite_state.dart';

class FavoriteCubit extends Cubit<FavoriteState> {
  FavoriteCubit({
    required FavoritetUseCase favoriteUse,
    required PagingController<int, FavoriteModel> paginateController,
  })  : _favoriteUse = favoriteUse,
        _paginateController = paginateController,
        super(FavoriteState());

  final FavoritetUseCase _favoriteUse;
  final PagingController<int, FavoriteModel> _paginateController;
  PagingController<int, FavoriteModel> get paginateController => _paginateController;

  static const _pageSize = 10;

  Future<void> init() async {
    _paginateController.addPageRequestListener(_paginationListener);
  }

  void _paginationListener(int pageKey) async {
    final response = await _favoriteUse.getFavorites();
    if (response.message!.isNotEmpty) {
      _paginateController.error = response.message;
      return;
    }
    final isLastPage = response.data.length <= _pageSize;
    isLastPage
        ? _paginateController.appendLastPage(response.data)
        : _paginateController.appendPage(response.data, pageKey + 1);
  }

  void addFavorite(ProductModel product) {
    _paginateController.refresh();
    product.favorite = true;
    _favoriteUse.createFavorite(product);
  }
}
