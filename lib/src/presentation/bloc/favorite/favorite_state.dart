part of 'favorite_cubit.dart';

class FavoriteState {
  FavoriteState({
    this.favorite,
    this.favorites = const <ProductModel>[],
  });

  final ProductModel? favorite;
  final List<ProductModel> favorites;

  FavoriteState copyWith({
    ProductModel? favorite,
    List<ProductModel>? favorites,
  }) =>
      FavoriteState(
        favorite: favorite ?? this.favorite,
        favorites: favorites ?? this.favorites,
      );
}
