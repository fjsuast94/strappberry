import 'package:strappberry/src/presentation/core/navigator/routes.dart';
import 'package:strappberry/src/presentation/core/theme/theme_style.dart';
import 'package:strappberry/src/presentation/core/utils/i18.dart';
import 'package:strappberry/src/presentation/shared/logger/logger_utils.dart';
import 'package:strappberry/src/presentation/views/splash/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeStyle.init(context);

    return GetMaterialApp(
      title: 'Strappberry',
      debugShowCheckedModeBanner: false,
      getPages: Routes.routes,
      theme: ThemeStyle.lightTheme,
      locale: Get.deviceLocale,
      translations: I18(),
      defaultTransition: Transition.fadeIn,
      home: const SplashView(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (deviceLocale, supporteLocate) => supporteLocate.first,
      supportedLocales: const [
        Locale('es'), // Español
        Locale('en'), // English
      ],
      enableLog: true,
      logWriterCallback: Logger.write,
    );
  }
}
