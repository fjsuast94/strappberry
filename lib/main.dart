import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:strappberry/src/app.dart';
import 'package:strappberry/src/presentation/bloc/repositories/app_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  runApp(const AppRepository(child: App()));
}

Future<void> initServices() async {
  //print('starting services ...');
  HttpOverrides.global = MyHttpOverrides();
  await GetStorage.init();
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
